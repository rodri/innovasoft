package ch.innovasoft.test;

import com.android.volley.toolbox.NetworkImageView;

import ch.innovasoft.test.data.Photo;
import ch.innovasoft.test.imageloader.MyImageLoader;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class DetailActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		
		callService();
	}
	
	private void callService() {
		
		Intent intent = getIntent();
		Photo photo = (Photo) intent.getSerializableExtra(MainActivity.photoKey);
		
		String url = "https://c1.staticflickr.com/"+ photo.getFarm() + "/" 
				+ photo.getServer() + "/"+ photo.getId() 
				+"_" + photo.getSecret() + "_c.jpg";

		NetworkImageView ivPhoto = (NetworkImageView) findViewById(R.id.ivPhoto);
		ivPhoto.setImageUrl(url, MyImageLoader.getImageLoader());
	}
}

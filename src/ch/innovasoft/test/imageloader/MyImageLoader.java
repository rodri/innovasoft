package ch.innovasoft.test.imageloader;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class MyImageLoader {

	private static RequestQueue sRequestQueue;
	private static ImageLoader mImageLoader;

	public static void init(final Context context) {
		sRequestQueue = Volley.newRequestQueue(context);

	}

	public static void exec(final Request<?> request) {
		sRequestQueue.add(request);
	}

	public static void cancelAll(final Object tag) {
		sRequestQueue.cancelAll(tag);
	}

	public static ImageLoader getImageLoader() {

		if (mImageLoader == null) {
			mImageLoader = new ImageLoader(sRequestQueue, new LruBitmapCache());
		}
		return mImageLoader;
	}
}

package ch.innovasoft.test;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;
import ch.innovasoft.test.adapter.PhotosAdapter;
import ch.innovasoft.test.data.PhotoSvcResponse;
import ch.innovasoft.test.data.Photos;
import ch.innovasoft.test.imageloader.MyImageLoader;
import ch.innovasoft.test.network.VolleyGsonRequest;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

public class MainActivity extends ListActivity {

	private Photos photos;
	private PhotosAdapter adapter;
	private ProgressBar progressBar;
	public static final String photoKey = "photo";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		progressBar = (ProgressBar) findViewById(R.id.progress_bar);

		MyImageLoader.init(this);
		callService();
		setClickListener();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void callService() {
		
		progressBar.setVisibility(View.VISIBLE);
		getListView().setVisibility(View.GONE);

		String url = "https://api.flickr.com/services/rest/?method=flickr.people.getPublicPhotos"
				+ "&api_key=342f56a6e75096b48983e6290ff7ff92"
				+ "&user_id=127953814@N07" + "&format=json";

		// initialize request
		VolleyGsonRequest volleyRequest = new VolleyGsonRequest(
				Request.Method.GET, url, null, PhotoSvcResponse.class,
				new Response.Listener() {
					@Override
					public void onResponse(Object response) {
						
						progressBar.setVisibility(View.GONE);
						getListView().setVisibility(View.VISIBLE);

						photos = ((PhotoSvcResponse) response).getPhotos();

						if (photos.getPhoto() != null) {

							if (adapter == null) {

								adapter = new PhotosAdapter(MainActivity.this,
										photos.getPhoto());
								setListAdapter(adapter);
							} else {
								adapter.setPhotos(photos.getPhoto());
								adapter.notifyDataSetChanged();
							}
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						
						progressBar.setVisibility(View.GONE);
						getListView().setVisibility(View.VISIBLE);

						Toast.makeText(
								MainActivity.this,
								MainActivity.this.getResources().getString(
										R.string.error_getting_photos),
								Toast.LENGTH_SHORT).show();

					}
				}, this);

		// initialize queue
		RequestQueue queue = Volley.newRequestQueue(this);
		volleyRequest.setShouldCache(true);
		// make the call
		queue.add(volleyRequest);
	}

	private void setClickListener() {

		getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Intent intent = new Intent(MainActivity.this,
						DetailActivity.class);
				intent.putExtra(photoKey, photos.getPhoto().get(position));

				startActivity(intent);
			}
		});
	}
}

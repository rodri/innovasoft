package ch.innovasoft.test.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import ch.innovasoft.test.R;
import ch.innovasoft.test.data.Photo;
import ch.innovasoft.test.imageloader.MyImageLoader;

import com.android.volley.toolbox.NetworkImageView;

public class PhotosAdapter extends BaseAdapter {

	private final Context mContext;
	private ViewHolder holder;
	private List<Photo> photos;

	public PhotosAdapter(final Context context, List<Photo> photos) {
		mContext = context;

		this.photos = photos;
	}

	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}

	@Override
	public int getCount() {
		return photos.size();
	}

	@Override
	public Object getItem(int position) {
		return photos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View view, ViewGroup viewGroup) {

		if (null == view) {
			view = LayoutInflater.from(mContext).inflate(R.layout.list_row,
					null);

			holder = new ViewHolder(view);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		String url = "https://c1.staticflickr.com/"
				+ photos.get(position).getFarm() + "/"
				+ photos.get(position).getServer() + "/"
				+ photos.get(position).getId() + "_"
				+ photos.get(position).getSecret() + "_c.jpg";

		holder.ivPhoto.setImageUrl(url, MyImageLoader.getImageLoader());
		holder.ivPhoto.setDefaultImageResId(R.drawable.ic_launcher);

		return view;
	}

	private static class ViewHolder {

		public final NetworkImageView ivPhoto;

		public ViewHolder(final View view) {
			ivPhoto = (NetworkImageView) view.findViewById(R.id.ivPhoto);
		}

	}

}
